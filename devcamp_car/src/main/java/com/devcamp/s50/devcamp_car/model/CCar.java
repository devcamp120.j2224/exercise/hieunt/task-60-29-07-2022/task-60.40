package com.devcamp.s50.devcamp_car.model;

import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "car")
public class CCar {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id ;

    @Column(name = "car_code", unique = true)
    private String carCode;

    @Column(name = "car_name")
    private String carName;

    @OneToMany(mappedBy = "car", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<CCarType> carTypes ;

    public CCar() {
    }

    public CCar(int id, String carCode, String carName, Set<CCarType> carTypes) {
        this.id = id;
        this.carCode = carCode;
        this.carName = carName;
        this.carTypes = carTypes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCarCode() {
        return carCode;
    }

    public void setCarCode(String carCode) {
        this.carCode = carCode;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public Set<CCarType> getCarTypes() {
        return carTypes;
    }

    public void setCarTypes(Set<CCarType> carTypes) {
        this.carTypes = carTypes;
    }

}
